<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

		<?php if ($slider = get_field('slider','option')): ?>
			<div class="home__slider--holder <?= get_field('chat','option') && get_field('chat_on','option') ? 'chat' : ''; ?>">
				<div class="home__slider slider_JS">
					<?php foreach ($slider as $g): ?>
						<div 
						class="home__slider--item" 
						style="background-image: url(<?= wp_get_attachment_image_src($g['id'],'full')[0] ?>);">
							<span></span>
						</div>
					<?php endforeach ?>
				</div>
			</div>
		<?php endif ?>

		<section class="home">
			<div class="home--wrap">

				<?php if (get_field('stream','option') && get_field('stream_on','option')): ?>
					<div class="home__stream">
						<?= get_field('stream','option') ?>
					</div>
				<?php else: ?>
					<div class="home__main">
						<?php if ($logo = get_field('logo','option')): ?>
							<img class="home__main--logo player_btn_JS" src="<?= wp_get_attachment_image_src($logo,'large')[0] ?>" alt="<?php bloginfo('name') ?>" />
						<?php endif ?>
						<?php if ($player = get_field('player','option')): ?>
							<div class="home__main--player block player_btn_JS">
								<i></i><span></span>
							</div>
							<audio class="player_JS">
								<source src="<?= get_field('player','option') ?>">
							</audio>
						<?php endif ?>
					</div>
				<?php endif ?>
				<?php if (get_field('slider','option') && count(get_field('slider','option')) > 1): ?>
					<div class="home__main--dots slider_dots_JS"></div>
				<?php endif ?>
				<?php if ($social = get_field('social','option')): ?>
					<div class="home__social">
						<?php foreach ($social as $s): ?>
							<a 
							target="_blank" 
							href="<?= $s['link'] ?>" 
							class="home__social--item fa <?= $s['icon'] ?>"></a>
						<?php endforeach ?>
					</div>
				<?php endif ?>
				<?php if ($info = get_field('info','option')): ?>
					<div class="home__info">
						<div class="home__info--content info_JS">
							<div class="home__info--ttl"><?php bloginfo('name') ?></div>
							<div class="home__info--txt"><?= get_field('info','option') ?></div>
						</div>
						<div class="home__info--btn info_btn_JS"></div>
					</div>
				<?php endif ?>
				<div class="home--copy">
					Transmitiendo con <a href="http://mensajito.mx" target="_blank">mensajito™</a> 
				</div>

				<?php if (get_field('chat','option') && get_field('chat_on','option')): ?>
					<div class="home__chat"><?= get_field('chat','option') ?></div>
				<?php endif ?>

			</div>
		</section>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>
