
<?php get_header(); ?>


	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
	
	<div id="single" class="post">
		
		<div class="block">
		
			<h2 class="tgray t38 exosemi"><?php the_title(); ?></h2>
			
			<div class="meta exoregular">
			<?php $cat = get_the_category(); $cat = $cat[0]; echo $cat->cat_name; ?> &mdash;  <?php the_time('d.m.Y') ?>
			</div>
			
			<br /><br />
			
			<div class="full"><?php the_content(); ?></div>
			
			<br /><br />
						
		</div>
		
		<div class="number exobold"><div class="pad">&nbsp;</div></div>
		
		<div class="clear"></div>
		
	</div>
	
	<?php endwhile; endif; ?>

<?php get_footer(); ?>

