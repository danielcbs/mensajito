<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>

<head profile="http://gmpg.org/xfn/11">
	
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"/>

	<title>
		<?php if (is_front_page()): ?>
			<?php bloginfo('name'); ?>
		<?php else: ?>
			<?php bloginfo('name'); ?> | <?php the_title() ?>
		<?php endif ?>
	</title>

	<meta name="description" content="<?php bloginfo('description'); ?>" />
	
	<?php
	if (get_the_post_thumbnail_url()) {
		$share_img = get_the_post_thumbnail_url();
	} elseif (get_field('img','option')) {
		$share_img = get_field('img','option');
	} else {
		$share_img = get_bloginfo('template_url').'/screenshot.png';
	}
	?>

	<!-- Twitter Card data -->
	<meta name="twitter:card" content="summary">
	<meta name="twitter:site" content="@">
	<meta name="twitter:title" content="<?php bloginfo('name'); ?> <?= (is_singular() || is_page()) && !is_front_page() ? ' | '.get_the_title() : '' ; ?> <?php if (get_bloginfo('description')): ?> <?= is_front_page() ? ' | '.get_bloginfo('description') : '' ; ?> <?php endif ?>">
	<meta name="twitter:description" content="<?php if (is_singular()) { echo strip_tags(get_the_content()); } else { echo bloginfo('description'); } ?>">
	<meta name="twitter:creator" content="@">
	<meta name="twitter:image" content="<?= wp_get_attachment_image_src($share_img,'full')[0] ?>">

	<!-- Open Graph data -->
	<meta property="og:title" content="<?php bloginfo('name'); ?> <?= (is_singular() || is_page()) && !is_front_page() ? ' | '.get_the_title() : '' ; ?> <?php if (get_bloginfo('description')): ?> <?= is_front_page() ? ' | '.get_bloginfo('description') : '' ; ?> <?php endif ?>">
	<meta property="og:type" content="article" />
	<meta property="og:url" content="<?php the_permalink(); ?>" />
	<meta property="og:image" content="<?= wp_get_attachment_image_src($share_img,'full')[0] ?>"/>
	<meta property="og:description" content="<?php if (is_singular()) { echo strip_tags(get_the_content()); } else { echo bloginfo('description'); } ?>"/>
	<meta property="og:site_name" content="<?php bloginfo('name'); ?>" />
	<meta property="fb:admins" content="" />
	
	<?php if ($favicon = get_field('favicon','option')): $favicon_meta = wp_get_attachment_metadata($favicon); ?>
		<link rel="apple-touch-icon-precomposed" href="<?= wp_get_attachment_image_src($favicon,'full')[0] ?>" sizes="<?= $favicon_meta['width'],'x',$favicon_meta['height'] ?>" />
		<link rel="icon" type="image/png" href="<?= wp_get_attachment_image_src($favicon,'full')[0] ?>" sizes="<?= $favicon_meta['width'],'x',$favicon_meta['height'] ?>" />
		<meta name="application-name" content="<?php bloginfo('name'); ?>"/>
		<meta name="msapplication-TileColor" content="#000000" />
		<meta name="msapplication-TileImage" content="<?= wp_get_attachment_image_src($favicon,'full')[0] ?>" />
	<?php else: ?>
		<?php include_once('inc/general/favicon.php'); ?>
	<?php endif ?>
	
	<link rel="stylesheet" href="<?php bloginfo('template_url') ?>/css/style.css?<?= date('j-m-y-h:i:s'); ?>" type="text/css" />
	
	<?php wp_head(); ?>

</head>

<body>

