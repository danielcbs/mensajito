<?php get_header(); ?>
	
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<section class="home">
			<div class="home--wrap wrap">
				
				<div class="home__main">
					<?php if ($logo = get_field('logo','option')): ?>
						<img class="home__main--logo" src="<?= wp_get_attachment_image_src($logo,'large')[0] ?>" alt="<?php bloginfo('name') ?>" />
					<?php endif ?>
					<div class="home__main--player">
						<span>Reproducir</span> <i class="fa fa-play"></i>
					</div>
					<div class="home__main--dots slider_dots_JS"></div>
				</div>

				<?php if ($slider = get_field('slider','option')): ?>
					<div class="home__slider--holder">
						<div class="home__slider slider_JS">
							<?php foreach ($slider as $g): ?>
								<div 
								class="home__slider--item" 
								style="background-image: url(<?= wp_get_attachment_image_src($g['id'],'xl')[0] ?>);">
									<span></span>
								</div>
							<?php endforeach ?>
						</div>
					</div>
				<?php endif ?>

				<?php if ($social = get_field('social')): ?>
					<div class="home__social">
						<?php foreach ($social as $s): ?>
							<a 
							target="_blank" 
							href="<?= $s['link'] ?>" 
							class="home__social--item fa <?= $s['icon'] ?>"></a>
						<?php endforeach ?>
					</div>
				<?php endif ?>

			</div>
		</section>
	<?php endwhile; endif; ?>

<?php get_footer(); ?>
