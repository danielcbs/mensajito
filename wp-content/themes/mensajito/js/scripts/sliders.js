// jshint asi: true
// jshint devel: true
// jshint unused:false

function sliders() {

	$('.slider_JS').slick({
		dots: true,
		fade: true,
		speed: 600,
		arrows: false,
		autoplay: true,
		appendDots: '.slider_dots_JS',
		pauseOnHover: false,
		autoplaySpeed: 5000,
	})

}