// jshint asi: true
// jshint devel: true
// jshint unused:false

// @codekit-prepend "scripts/sliders.js"

$(document).ready(function() {
	
	// Sliders
	sliders()

	// Info
	$('.info_btn_JS').click(function () {
		$(this).toggleClass('active')
		$('.info_JS').toggleClass('active')
	})

	// Player
	$('.player_btn_JS').click(function() {
		if (!$(this).hasClass('active')) {
			$('.player_JS').trigger('play')
			$('.player_btn_JS').toggleClass('active')
		} else {
			$('.player_JS').trigger('pause')
			$('.player_btn_JS').toggleClass('active')
		}
	})

	// var player = $('.player_JS source').attr('src')
	// $('.player_JS').load(player,function(response,status,xhr) {
	// 	if (status === 'error') {
	// 		$(this).addClass('off')
	// 	}
	// })

})

$(window).load(function() {
	$('.player_btn_JS').removeClass('block')
})
