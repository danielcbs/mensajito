<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<section class="rsvp" style="background-image:url(<?php bloginfo('template_url') ?>/img/background.jpg);background-size:cover;">
			<div class="rsvp--wrap wrap">

				<img class="rsvp--header" src="<?php bloginfo('template_url') ?>/img/intro-4.png">
				<div class="rsvp__content">
					<div class="rsvp__content--intro" style="display:none;">
						<p>&nbsp;</p>
						<p style="text-align: center;">
						<strong>REGISTRO CERRADO</strong><br />
						Te esperamos este Viernes 23 de Agosto con <br/>
						Nick Hook (NYC) + 1OO1O (CDMX) + BCOTB</p>

						<p style="text-align: center;">
							A todos los que se registraron, enviamos a su email la <a style="color:black;" href="https://www.google.com/maps/place/Agustín+de+Iturbide+640,+Centro+Historico,+78000+San+Luis,+S.L.P./@22.1506122,-100.9791163,17z/data=!3m1!4b1!4m5!3m4!1s0x842aa201bbb24be7:0x40216e29dab8c9b3!8m2!3d22.1506122!4d-100.9769276" target="_blank"><u>locación</u></a>. <br />
						</p>
						<p style="text-align: center;">
						<strong>*No Cover antes de las 11 PM, después $150<br />
						*<u>Acceso únicamente con invitación</u></strong></p>
					</div>
					<div class="rsvp__content--intro">
						<p>&nbsp;</p>
						<p style="text-align: center;">
						Regístrate para asistir a: <br/>
						<strong>Zhino (GDL) + Ruiseñor + DJ Fucci + Cecilia</strong> <br/>
						este Jueves 5 de Diciembre de 2019
						</p>
						<p style="text-align: center;">
						*<u>Acceso únicamente con registro</u></strong></p>
					</div>
					<div class="rsvp__content--mailchimp">
						<div id="mc_embed_signup">
							<form action="https://movelike.us12.list-manage.com/subscribe/post?u=c500823c68c34b818c1a413f6&amp;id=04a4e63612" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
								<div id="mc_embed_signup_scroll">
									<div class="mc-field-group">
										<label for="mce-FNAME">Nombre </label>
										<input type="text" value="" name="FNAME" class="required" id="mce-FNAME">
									</div>
									<div class="mc-field-group">
										<label for="mce-EMAIL">Email </label>
										<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
									</div>
									<div id="mce-responses" class="clear">
										<div class="response" id="mce-error-response" style="display:none"></div>
										<div class="response" id="mce-success-response" style="display:none"></div>
									</div><!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
									<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_c500823c68c34b818c1a413f6_04a4e63612" tabindex="-1" value=""></div>
									<div class="clear"><input style="background:blue;" type="submit" value="Registrarme" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
								</div>
							</form>
						</div>
					</div>
				</div>
				<div class="rsvp__social">
					<a class="rsvp__social--item fa fa-instagram" href="https://instagr.am/movelike.co" target="_blank"><span>Instagram</span></a>
					<a class="rsvp__social--item fa fa-facebook" href="https://instagr.am/movelike.co" target="_blank"><span>Facebook</span></a>
				</div>

			</div>
		</section>
	<?php endwhile; endif; ?>

<?php get_footer(); ?>
