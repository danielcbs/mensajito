<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'mensajito' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         's=dQd7:C$puIYLqv#d|y<|WA|_+LQU*_Zid!C`3e!$RpOf(P&3We!PnSX5_is<Db' );
define( 'SECURE_AUTH_KEY',  'sq<%+91Z(+6FSI7KDxX%sw&y)&vEO&5CVMpDyLSHLFJz|!9P=Q[DTsr;<LjWBM%i' );
define( 'LOGGED_IN_KEY',    ':7EmXKPmmyQ5^axgv$epb-zAZQ||sdYudxMF2zDmO!=J>[NK:6pADKAiRgRI_{$|' );
define( 'NONCE_KEY',        '%R<!4Ty&eB )7UE<WWA@prJh/ekmk*9DXZP=U9~tx4+n8`qf.HfrK5gYwY^.bLo(' );
define( 'AUTH_SALT',        'R=dcu rn^ K$TZG*0+ODJ9Mp75TE6jxSaxr3QT{q,krGU=Es.e0Q4[W*yWNu0{Q-' );
define( 'SECURE_AUTH_SALT', 'b%178+J=!6.g~)iKA51t6A8*@xjx55rrYC!&IDR1=@L1`2#hw$^K/qYv|OlZ:I+O' );
define( 'LOGGED_IN_SALT',   'H+0%,j#9YXHqb 3O @.y/v+/jM^U{* @M89xUD5+!xhg2G<>6l?mbl~8L=}UIU[]' );
define( 'NONCE_SALT',       'vyhtUWLLz?![S>+qf@zY+QO}>:[_#l9,Fiq$:x}n]2EM3($e]xx[@K1pv/+e9XDh' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
